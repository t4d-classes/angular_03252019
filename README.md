# Welcome to Angular Class!

## Instructor

Eric Greene

## Schedule

Class:

- Monday - Friday: 9:00am to 5:00pm PDT

Breaks:

- Morning Break: 10:25am to 10:35am
- Lunch: 12:00pm to 1pm
- Afternoon Break: 2:15pm to 2:25pm
- Afternoon Break: 3:40pm to 3:50pm

## Course Outline

- Day 1 - Overview of Angular, Angular CLI, Modules, Components
- Day 2 - Component Composition, Pipes
- Day 3 - Reactive Forms, Services
- Day 4 - Asynchronous Programming, REST Service Calls, RxJS, NgRX
- Day 5 - Routing, Unit Testing, Review Project Code

## Links

### Instructor's Resources

- [Accelebrate, Inc.](https://www.accelebrate.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016

### Other Resources

- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS)
- [JavaScript Air Podcast](http://javascriptair.podbean.com/)
- [Speaking JavaScript](http://speakingjs.com/es5/)

## Useful Resources

- [Angular CLI](https://cli.angular.io/)
- [TypeScript Coding Guidelines](https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines)
- [Angular Style Guide](https://angular.io/docs/ts/latest/guide/style-guide.html)
- [Angular Cheat Sheet](https://angular.io/docs/ts/latest/guide/cheatsheet.html)
- [Angular API](https://angular.io/docs/ts/latest/api/)
