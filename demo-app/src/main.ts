import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

// import { Observable, Observer, Subscription, concat, merge, interval } from 'rxjs';
// import { take, map } from 'rxjs/operators';

// merge(
//   interval(500).pipe(take(10)),
//   interval(500).pipe(take(10), map(num => num * 1000)),
// ).subscribe(num => {
//   console.log(num);
// });



// const nums$: Observable<number> = Observable.create( (observer: Observer<number>) => {

//   console.log('starting observer');

//   let counter = 0;
  
//   const handle = setInterval(() => {

//     if (observer.closed) {
//       clearInterval(handle);
//     }

//     console.log('Ling is awesome!');
//     observer.next(counter++);
    
//   }, 1000);

//   // setTimeout(() => {
//   //   clearInterval(handle);
//   //   observer.error('something went wrong');
//   // }, 5000);

//   setTimeout(() => {
//     clearInterval(handle);
//     observer.complete();
//   }, 10000);

// } );

// const subscription: Subscription = nums$.subscribe(
//   num => { console.log(num); },
//   (err) => { console.log('err', err); },
//   () => { console.log('all done'); },
// );

// setTimeout(() => {
//   subscription.unsubscribe();
// }, 5000);

