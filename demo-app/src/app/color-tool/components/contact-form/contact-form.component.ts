import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { AppConfigService } from '../../../shared/services/app-config.service';

const myRequired = (c: FormControl) => {

  if (c.value == null || String(c.value).length === 0) {
    return {
      myRequired: true,
    };
  }

  return null;
};

const emailOrPhoneValidator = (g: FormGroup) => {

  if (!g.get('emailAddress').value && !g.get('phoneNumber').value) {
    return {
      emailOrPhoneOrLing: true,
      message: 'It failed',
    };
  }

  return null;
};


const nameAsyncValidator = (httpClient: HttpClient, appConfigSvc: AppConfigService) => (g: FormGroup) => {

  const firstName = g.get('firstName').value;
  const lastName = g.get('lastName').value;

  console.log(appConfigSvc.baseURL);

  return httpClient
    .get<object[]>(`${appConfigSvc.baseURL}/people?name=${firstName}&lastName=${lastName}`)
    .pipe(map( (result: object[]) => result.length === 0 ? { name: 'First name and last name not found.'} : null))
    .toPromise();

};

@Component({
  selector: 'contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {

  contactForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private httpClient: HttpClient,
    private appConfigSvc: AppConfigService,
  ) { }

  get firstNameValid() {
    // console.log('called first name valid');
    const firstNameControl = this.contactForm.get('firstName') as FormControl;
    return firstNameControl.touched && firstNameControl.invalid;
  }

  states = [
    { code: 'CA', name: 'California' },
    { code: 'NY', name: 'New York' },
    { code: 'VA', name: 'Virginia' },
  ];

  ngOnInit() {

    this.contactForm = this.fb.group({
      firstName: [
        '',
        { validators: [ Validators.required ], updateOn: 'blur' },
      ],
      lastName: [
        '',
        { validators: [ myRequired ], updateOn: 'blur' },
      ],
      address: this.fb.group({
        street: '',
        city: '',
        state: '',
        zipCode: '',
      }),
      contactMethod: this.fb.group({
        mayContact: false,
        preferredContactMethod: 'email',
        emailAddress: '',
        phoneNumber: '',
      }, { validators: [ emailOrPhoneValidator ] }),
      comments: '',
      todos: this.fb.array([ 'task 1', 'task 2', 'task 3' ]),
    }, { asyncValidators: [ nameAsyncValidator(this.httpClient, this.appConfigSvc) ]});
  }

  doAddTask() {
    const todosArr = this.contactForm.get('todos') as FormArray;
    todosArr.push(new FormControl(''));
  }

  doSend() {
    console.log(this.contactForm.value);
  }

}
