import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { SharedModule } from '../shared/shared.module';

import { ColorHomeComponent } from './components/color-home/color-home.component';
import { ColorFormComponent } from './components/color-form/color-form.component';
import { ContactFormComponent } from './components/contact-form/contact-form.component';

@NgModule({
  declarations: [ ColorHomeComponent, ColorFormComponent, ContactFormComponent ],
  exports: [ ColorHomeComponent, ContactFormComponent ],
  imports: [
    CommonModule, ReactiveFormsModule, HttpClientModule, SharedModule,
  ]
})
export class ColorToolModule { }
