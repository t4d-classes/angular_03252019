import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Car } from '../models/car';

@Injectable({
  providedIn: 'root'
})
export class CarsService {

  constructor(private httpClient: HttpClient) { }

  all() {
    return this.httpClient.get<Car[]>('http://localhost:4250/cars', {
      headers: { Authorization: 'testauth' },
    });
  }

  append(car: Car) {
    return this.httpClient.post<Car>('http://localhost:4250/cars', car, {
      headers: { Authorization: 'testauth' },
    });
  }

  replace(car: Car) {
    return this.httpClient.put<void>(
      `http://localhost:4250/cars/${encodeURIComponent(car.id.toString())}`,
      car,
      { headers: { Authorization: 'testauth' } });
  }

  delete(carId: number) {
    return this.httpClient.delete<void>(
      `http://localhost:4250/cars/${encodeURIComponent(carId.toString())}`,
      { headers: { Authorization: 'testauth' } });
  }

}
