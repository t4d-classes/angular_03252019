import { Component, OnInit} from '@angular/core';
import { switchMap } from 'rxjs/operators';

import { Car } from '../../models/car';
import { CarsService } from '../../services/cars.service';

@Component({
  selector: 'car-home',
  templateUrl: './car-home.component.html',
  styleUrls: ['./car-home.component.css']
})
export class CarHomeComponent implements OnInit {

  cars: Car[] = [];

  editCarId = -1;

  constructor(private carsSvc: CarsService) { }

  ngOnInit() {
    this.carsSvc.all()
      .subscribe(cars => this.cars = cars);
  }

  doAppendCar(car: Car) {
    this.carsSvc.append(car).pipe(
      switchMap(() => this.carsSvc.all())
    ).subscribe(cars => this.cars = cars);
    this.editCarId = -1;
  }

  doEditCar(carId: number) {
    this.editCarId = carId;
  }

  doCancelCar() {
    this.editCarId = -1;
  }

  doReplaceCar(car: Car) {
    this.carsSvc.replace(car).pipe(
      switchMap(() => this.carsSvc.all())
    ).subscribe(cars => this.cars = cars);
    this.editCarId = -1;
  }

  doDeleteCar(carId: number) {
    this.carsSvc.delete(carId).pipe(
      switchMap(() => this.carsSvc.all())
    ).subscribe(cars => this.cars = cars);
    this.editCarId = -1;
  }
}
