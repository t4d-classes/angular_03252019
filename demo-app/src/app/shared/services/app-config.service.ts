import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
  useValue: {
    baseURL: 'http://localhost:4250'
  }
})
export class AppConfigService {

  constructor() { }

  baseURL: string;
}
