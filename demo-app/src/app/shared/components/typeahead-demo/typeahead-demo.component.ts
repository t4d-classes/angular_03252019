import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, BehaviorSubject, } from 'rxjs';
import { switchMap, debounceTime } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

interface Person {
  id: number;
  name: string;
  ssn: string;
}

@Component({
  selector: 'typeahead-demo',
  templateUrl: './typeahead-demo.component.html',
  styleUrls: ['./typeahead-demo.component.css']
})
export class TypeaheadDemoComponent implements OnInit {

  ssnInput$ = new BehaviorSubject<string>('');

  peopleResults$: Observable<Person[]> = this.ssnInput$.pipe(
    debounceTime(500),
    switchMap(ssnValue => {
      return this.httpClient
        .get<Person[]>(`http://localhost:4250/people?ssn_like=${encodeURIComponent(ssnValue)}`);
    }),
  );

  ssnSearchInput = new FormControl('');

  constructor(private httpClient: HttpClient) { }

  ngOnInit() {
  }
 
  doSSNSearch() {
    this.ssnInput$.next(this.ssnSearchInput.value);
  }
}
