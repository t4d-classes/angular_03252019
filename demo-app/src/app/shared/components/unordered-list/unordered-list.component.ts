import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'unordered-list',
  templateUrl: './unordered-list.component.html',
  styleUrls: ['./unordered-list.component.css']
})
export class UnorderedListComponent implements OnInit {

  @Input() // props
  items: string[];

  constructor() { }

  ngOnInit() {
    this.items = this.items.concat('test');
  }


}
