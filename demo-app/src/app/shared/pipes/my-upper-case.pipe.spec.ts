import { MyUpperCasePipe } from './my-upper-case.pipe';

describe('MyUpperCasePipe', () => {

  let pipe: MyUpperCasePipe;

  beforeEach(() => {
    pipe = new MyUpperCasePipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should make text uppercase', () => {
    const output = pipe.transform('abc');
    expect(output).toEqual('ABC');
  });
});