import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'myUpperCase',
  pure: true, // default value
})
export class MyUpperCasePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    console.log('my uppercase pipe is running');
    const sValue = String(value);
    return sValue.toUpperCase();
  }

}
