import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { ToolHeaderComponent } from './components/tool-header/tool-header.component';
import { UnorderedListComponent } from './components/unordered-list/unordered-list.component';
import { MyUpperCasePipe } from './pipes/my-upper-case.pipe';
import { MyAppendPipe } from './pipes/my-append.pipe';
import { TypeaheadDemoComponent } from './components/typeahead-demo/typeahead-demo.component';

@NgModule({
  declarations: [ToolHeaderComponent, UnorderedListComponent, MyUpperCasePipe, MyAppendPipe, TypeaheadDemoComponent],
  exports: [ToolHeaderComponent, UnorderedListComponent, MyUpperCasePipe, MyAppendPipe, TypeaheadDemoComponent],
  imports: [
    CommonModule, ReactiveFormsModule, HttpClientModule,
  ]
})
export class SharedModule { }
