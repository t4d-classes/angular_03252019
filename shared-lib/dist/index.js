'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var _angular_core = require('@angular/core');
var _angular_common = require('@angular/common');

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */
/* global Reflect, Promise */







function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

var CapitalizePipe = /** @class */ (function () {
    function CapitalizePipe() {
    }
    CapitalizePipe.prototype.transform = function (value) {
        return value.slice(0, 2).toUpperCase() + value.slice(2);
    };
    CapitalizePipe = __decorate([
        _angular_core.Pipe({
            name: 'capitalize',
        })
    ], CapitalizePipe);
    return CapitalizePipe;
}());

var AppMenuComponent = /** @class */ (function () {
    function AppMenuComponent() {
    }
    AppMenuComponent = __decorate([
        _angular_core.Component({
            selector: "app-menu",
            template: "<div>This is cool!</div>",
            styles: ["div {\n  color: green;\n}"],
        })
    ], AppMenuComponent);
    return AppMenuComponent;
}());

var Cars = /** @class */ (function () {
    function Cars() {
    }
    Cars.prototype.getAll = function () {
        return [
            { id: 1, make: 'Ford', model: 'Fusion', color: 'yellow', year: 2013, price: 12000 },
            { id: 2, make: 'Ford', model: 'F150', color: 'purple', year: 2014, price: 34000 },
        ];
    };
    Cars = __decorate([
        _angular_core.Injectable()
    ], Cars);
    return Cars;
}());

var Car = /** @class */ (function () {
    function Car() {
    }
    return Car;
}());

// TODO: Import and register below the Angular parts to be included in the module
// TODO: be sure to update the Angular module name to reflect the name of your package
var SharedLibModule = /** @class */ (function () {
    function SharedLibModule() {
    }
    SharedLibModule = __decorate([
        _angular_core.NgModule({
            imports: [_angular_common.CommonModule],
            declarations: [CapitalizePipe, AppMenuComponent],
            providers: [Cars],
            exports: [CapitalizePipe, AppMenuComponent],
        })
    ], SharedLibModule);
    return SharedLibModule;
}());

exports.CapitalizePipe = CapitalizePipe;
exports.AppMenuComponent = AppMenuComponent;
exports.Cars = Cars;
exports.Car = Car;
exports.SharedLibModule = SharedLibModule;
exports['default'] = SharedLibModule;
//# sourceMappingURL=index.js.map
