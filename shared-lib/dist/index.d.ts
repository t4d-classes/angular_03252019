import { CapitalizePipe } from "./pipes/capitalize.pipe";
import { AppMenuComponent } from "./components/menu/menu.component";
import { Cars } from "./services/cars.service";
import { Car } from "./models/car";
export { CapitalizePipe, AppMenuComponent, Cars, Car, };
export declare class SharedLibModule {
}
export default SharedLibModule;
