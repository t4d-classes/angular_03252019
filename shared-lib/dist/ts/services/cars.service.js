var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { Injectable } from "@angular/core";
var Cars = /** @class */ (function () {
    function Cars() {
    }
    Cars.prototype.getAll = function () {
        return [
            { id: 1, make: 'Ford', model: 'Fusion', color: 'yellow', year: 2013, price: 12000 },
            { id: 2, make: 'Ford', model: 'F150', color: 'purple', year: 2014, price: 34000 },
        ];
    };
    Cars = __decorate([
        Injectable()
    ], Cars);
    return Cars;
}());
export { Cars };
//# sourceMappingURL=cars.service.js.map