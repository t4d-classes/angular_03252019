var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
// TODO: Import and register below the Angular parts to be included in the module
import { CapitalizePipe } from "./pipes/capitalize.pipe";
import { AppMenuComponent } from "./components/menu/menu.component";
import { Cars } from "./services/cars.service";
import { Car } from "./models/car";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
export { CapitalizePipe, AppMenuComponent, Cars, Car, };
// TODO: be sure to update the Angular module name to reflect the name of your package
var SharedLibModule = /** @class */ (function () {
    function SharedLibModule() {
    }
    SharedLibModule = __decorate([
        NgModule({
            imports: [CommonModule],
            declarations: [CapitalizePipe, AppMenuComponent],
            providers: [Cars],
            exports: [CapitalizePipe, AppMenuComponent],
        })
    ], SharedLibModule);
    return SharedLibModule;
}());
export { SharedLibModule };
export default SharedLibModule;
//# sourceMappingURL=index.js.map