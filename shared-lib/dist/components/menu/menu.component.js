import { Component } from "@angular/core";
// import the HTML file and stylesheet with paths relative to this file
var AppMenuComponent = (function () {
    function AppMenuComponent() {
    }
    return AppMenuComponent;
}());
export { AppMenuComponent };
AppMenuComponent.decorators = [
    { type: Component, args: [{
                selector: "app-menu",
                templateUrl: './menu.component.html',
                styleUrls: ['./menu.component.css'],
            },] },
];
/** @nocollapse */
AppMenuComponent.ctorParameters = function () { return []; };
//# sourceMappingURL=menu.component.js.map