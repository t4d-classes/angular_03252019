import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, NavigationCancel } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit, OnDestroy {

  constructor(private router: Router) { }

  routeEventSubscription: Subscription;

  ngOnInit() {
    this.routeEventSubscription = this.router.events.subscribe( e => {
      if (e instanceof NavigationEnd) {
        this.routeEventSubscription.unsubscribe();
        console.log('navigation end');
      }
      if (e instanceof NavigationCancel) {
        console.log('navigation canceled');
      }
    });
  }

  ngOnDestroy() {
    // this.routeEventSubscription.unsubscribe();
  }

}
