import { Routes, RouterModule } from '@angular/router';

import {AuthGuardService} from './services/auth-guard.service';

import { HomePageComponent } from './components/home-page/home-page.component';
import { AboutPageComponent } from './components/about-page/about-page.component';
import { ContactPageComponent } from './components/contact-page/contact-page.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: HomePageComponent },
  { path: 'about', component: AboutPageComponent, canActivate: [ AuthGuardService ] },
  { path: 'contact', component: ContactPageComponent },
];

export const AppRouterModule = RouterModule.forRoot(
  routes,
  // { useHash: true },
);
