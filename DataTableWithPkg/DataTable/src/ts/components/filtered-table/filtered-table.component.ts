import {
  Component, ContentChild, Input, DoCheck
} from '@angular/core';

import { FilterForm } from '../../models/filter-form';
import { FilterTable} from '../../models/filter-table';

@Component({
  selector: 'filtered-table',
  templateUrl: './filtered-table.component.html',
  styleUrls: ['./filtered-table.component.css']
})
export class FilteredTableComponent implements DoCheck {

  @ContentChild(FilterForm)
  public filterForm: FilterForm;

  @ContentChild(FilterTable)
  public filterTable: FilterTable;

  @Input()
  public filterFn: (filterValue: string) =>
      (value: any, index: number, array: any[]) => boolean;

  public ngDoCheck() {
    this.filterTable.filteredData = this.filteredData;
  }

  public get filteredData(): any[] {
      return !this.filterForm.filterValue
        ? this.filterTable.allData
        : this.filterTable.allData.filter(this.filterFn(this.filterForm.filterValue));
  }

}
