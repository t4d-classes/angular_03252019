import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dataFormatPipe'
})
export class DataFormatPipePipe implements PipeTransform {

  transform(value: any, args?: any): any {

    switch (args) {
      case 'date':
        return new Date(value).toDateString();
      default:
        return value;
    }
  }

}
