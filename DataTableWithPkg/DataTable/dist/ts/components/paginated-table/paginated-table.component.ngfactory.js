/**
* @fileoverview This file is generated by the Angular template compiler.
* Do not edit.
* @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
* tslint:disable
*/ 
import * as i0 from "./paginated-table.component.css.shim.ngstyle";
import * as i1 from "@angular/core";
import * as i2 from "../../models/filter-table";
import * as i3 from "./paginated-table.component";
var styles_PaginatedTableComponent = [i0.styles];
var RenderType_PaginatedTableComponent = i1.ɵcrt({ encapsulation: 0, styles: styles_PaginatedTableComponent, data: {} });
export { RenderType_PaginatedTableComponent as RenderType_PaginatedTableComponent };
export function View_PaginatedTableComponent_0(_l) { return i1.ɵvid(0, [i1.ɵncd(null, 0), (_l()(), i1.ɵted(-1, null, ["\n"])), (_l()(), i1.ɵeld(2, 0, null, null, 10, "form", [], null, null, null, null, null)), (_l()(), i1.ɵted(-1, null, ["\n    "])), (_l()(), i1.ɵeld(4, 0, null, null, 1, "button", [["type", "button"]], [[8, "disabled", 0]], [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.go((0 - 1)) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), i1.ɵted(-1, null, ["Prev"])), (_l()(), i1.ɵted(-1, null, ["\n    "])), (_l()(), i1.ɵeld(7, 0, null, null, 1, "button", [["type", "button"]], [[8, "disabled", 0]], [[null, "click"]], function (_v, en, $event) { var ad = true; var _co = _v.component; if (("click" === en)) {
        var pd_0 = (_co.go(1) !== false);
        ad = (pd_0 && ad);
    } return ad; }, null, null)), (_l()(), i1.ɵted(-1, null, ["Next"])), (_l()(), i1.ɵted(-1, null, ["\n    "])), (_l()(), i1.ɵeld(10, 0, null, null, 1, "span", [], null, null, null, null, null)), (_l()(), i1.ɵted(11, null, ["\n        Page ", " of ", "\n    "])), (_l()(), i1.ɵted(-1, null, ["\n"])), (_l()(), i1.ɵted(-1, null, ["\n"]))], null, function (_ck, _v) { var _co = _v.component; var currVal_0 = _co.firstPage; _ck(_v, 4, 0, currVal_0); var currVal_1 = _co.lastPage; _ck(_v, 7, 0, currVal_1); var currVal_2 = (_co.currentPage + 1); var currVal_3 = _co.totalPages; _ck(_v, 11, 0, currVal_2, currVal_3); }); }
export function View_PaginatedTableComponent_Host_0(_l) { return i1.ɵvid(0, [(_l()(), i1.ɵeld(0, 0, null, null, 3, "paginated-table", [], null, null, null, View_PaginatedTableComponent_0, RenderType_PaginatedTableComponent)), i1.ɵprd(6144, null, i2.FilterTable, null, [i3.PaginatedTableComponent]), i1.ɵdid(2, 3260416, null, 1, i3.PaginatedTableComponent, [], null, null), i1.ɵqud(335544320, 1, { pageableTable: 0 })], function (_ck, _v) { _ck(_v, 2, 0); }, null); }
var PaginatedTableComponentNgFactory = i1.ɵccf("paginated-table", i3.PaginatedTableComponent, View_PaginatedTableComponent_Host_0, { data: "data", pageLength: "pageLength", initialPage: "initialPage", listPropName: "listPropName" }, {}, ["*"]);
export { PaginatedTableComponentNgFactory as PaginatedTableComponentNgFactory };
//# sourceMappingURL=paginated-table.component.ngfactory.js.map