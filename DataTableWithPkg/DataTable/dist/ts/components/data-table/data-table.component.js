var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
import { Component, Input, forwardRef } from '@angular/core';
import { PageableTable } from '../../models/pageable-table';
var DataTableComponent = (function (_super) {
    __extends(DataTableComponent, _super);
    function DataTableComponent() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.config = { cols: [] };
        return _this;
    }
    Object.defineProperty(DataTableComponent.prototype, "data", {
        get: function () {
            return this._pageOfData;
        },
        set: function (d) {
            this._allData = d;
            this._pageOfData = d;
        },
        enumerable: true,
        configurable: true
    });
    DataTableComponent.decorators = [
        { type: Component, args: [{
                    selector: 'data-table',
                    templateUrl: './data-table.component.html',
                    styleUrls: ['./data-table.component.css'],
                    providers: [
                        { provide: PageableTable, useExisting: forwardRef(function () { return DataTableComponent; }) },
                    ],
                },] },
    ];
    /** @nocollapse */
    DataTableComponent.ctorParameters = function () { return []; };
    DataTableComponent.propDecorators = {
        "config": [{ type: Input },],
        "data": [{ type: Input },],
    };
    return DataTableComponent;
}(PageableTable));
export { DataTableComponent };
//# sourceMappingURL=data-table.component.js.map