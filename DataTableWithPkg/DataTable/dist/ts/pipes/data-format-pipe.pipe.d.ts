import { PipeTransform } from '@angular/core';
export declare class DataFormatPipePipe implements PipeTransform {
    transform(value: any, args?: any): any;
}
