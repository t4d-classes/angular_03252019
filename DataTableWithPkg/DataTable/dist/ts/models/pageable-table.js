var PageableTable = (function () {
    function PageableTable() {
    }
    Object.defineProperty(PageableTable.prototype, "allData", {
        get: function () {
            return this._allData || [];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PageableTable.prototype, "dataPage", {
        set: function (d) {
            this._pageOfData = d;
        },
        enumerable: true,
        configurable: true
    });
    return PageableTable;
}());
export { PageableTable };
//# sourceMappingURL=pageable-table.js.map