import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FlightToolModule } from './flight-tool/flight-tool.module';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, FlightToolModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
