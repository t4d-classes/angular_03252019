import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightFilterFormComponent } from './flight-filter-form.component';

describe('FlightFilterFormComponent', () => {
  let component: FlightFilterFormComponent;
  let fixture: ComponentFixture<FlightFilterFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightFilterFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightFilterFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
