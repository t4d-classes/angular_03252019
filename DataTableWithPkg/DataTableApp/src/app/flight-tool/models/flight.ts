export interface Flight {
  flDate: number;
  carrier: string;
  tailNum: string;
  flNum: number;
  origin: string;
  dest: string;
  crsDepTime: number;
  depTime: number;
  crsArrTime: number;
  arrTime: number;
  carrierDelay: number;
  weatherDelay: number;
  nasDelay: number;
  securityDelay: number;
  lateAircraftDelay: number;
}
